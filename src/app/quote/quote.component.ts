import { Component, OnInit } from '@angular/core';
import { QuoteModal } from '../quote.modal';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { QuoteServices } from '../quote.services';
import { map } from 'rxjs';

@Component({
  selector: 'app-quote',
  templateUrl: './quote.component.html',
  styleUrls: ['./quote.component.css']
})
export class QuoteComponent implements OnInit {
  quote: QuoteModal[] | null = null;
  loader = false
  select = '';
  category = '';

  constructor(private http: HttpClient, public service: QuoteServices, private router: Router) {}

  ngOnInit(): void {

  }

  deleteQuote(index: string) {
    console.log(index);
    this.http.delete(`https://mlacvit-10af9-default-rtdb.firebaseio.com/quotes/someid${index}.json`).subscribe();
    void this.router.navigate(['/'])
  }

  getCategory() {
    this.service.getCategoryQuote(this.select);
  }

  addIdQuote(id: string){
    this.service.addId(id);
  }

}
