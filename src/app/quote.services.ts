
import { Injectable, OnInit } from '@angular/core';
import { QuoteModal } from './quote.modal';
import { map } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class QuoteServices implements OnInit{
    category = '';
    quote: QuoteModal[] | null = null;
    quoteAll: QuoteModal[] | null = null;
    quoteIdOne!: QuoteModal;
    quoteId!: string
constructor(private http: HttpClient) {}

  ngOnInit(): void{

  }

  getCategoryQuote(category: string){
    this.http.get<{ [id: string]: QuoteModal }>(`https://mlacvit-10af9-default-rtdb.firebaseio.com/quotes.json?orderBy="category"&equalTo="${category}"`)
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const data = result[id];
          return new QuoteModal(
            data.id,
            data.category,
            data.author,
            data.image,
            data.text,
          );
        });
      }))
      .subscribe(quote => {
        this.quote = quote;
      })
  }

  allQuote(){
    this.http.get<{ [id: string]: QuoteModal }>(`https://mlacvit-10af9-default-rtdb.firebaseio.com/quotes.json`)
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const data = result[id];
          return new QuoteModal(
            data.id,
            data.category,
            data.author,
            data.image,
            data.text,
          );
        });
      }))
      .subscribe(quote => {
        this.quote = quote;
        this.quoteAll = quote;
      });
  }

  addId(id: string) {
    this.http.get<QuoteModal>(`https://mlacvit-10af9-default-rtdb.firebaseio.com/quotes/someid${id}.json`)
      .pipe(map(result => {
          return new QuoteModal(
            result.id,
            result.category,
            result.author,
            result.image,
            result.text,
          );
      }))
      .subscribe(quote => {
        this.quoteIdOne = quote;
      });

  }

  deleteQuote(index: string) {
    console.log(index);
    this.http.delete(`https://mlacvit-10af9-default-rtdb.firebaseio.com/quotes/quote/${index}.json`).subscribe();
  }

}

