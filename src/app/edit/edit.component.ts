import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { QuoteServices } from '../quote.services';



@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  author = '';
  image = ''
  text = '';
  select = '';

  constructor(private http: HttpClient, public services: QuoteServices, private router: Router) {
  }

  ngOnInit(): void {
    };

    renameQuote(id: string){
      const author = this.author;
      const category = this.select;
      const image = this.image;
      const text = this.text;
      const body = {author, category, id, image, text};
      this.http.put(`https://mlacvit-10af9-default-rtdb.firebaseio.com/quotes.json?orderBy="id"&equalTo="id${id}"`, body).subscribe();

    }
  }

