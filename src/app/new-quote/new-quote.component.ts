import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-new-quote',
  templateUrl: './new-quote.component.html',
  styleUrls: ['./new-quote.component.css']
})
export class NewQuoteComponent implements OnInit {
  author = '';
  image = '';
  text = '';
  select = '';

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
  }

  newId(min: number, max: number) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  sendMessage() {
    const id = this.newId(10, 101)
    const author = this.author
    const image = this.image;
    const text = this.text;
    const category = this.select
    const body = {author, category, id, image, text};
    this.http.post(`https://mlacvit-10af9-default-rtdb.firebaseio.com/quotes.json`, body).subscribe()
  }
}
