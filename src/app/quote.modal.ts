export class QuoteModal {
  constructor(
    public id: string,
    public category: string,
    public author: string,
    public image: string,
    public text: string,
  ) {}
}
