import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { QuoteModal } from '../quote.modal';
import { HttpClient } from '@angular/common/http';
import { QuoteServices } from '../quote.services';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  quote = '';
  loader = false;
  id!: number;

  constructor(private route: ActivatedRoute, private http: HttpClient, public service: QuoteServices) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params)=> {
        this.service.getCategoryQuote(params['id']);
      console.log(params['id'])
    })


  }




}
