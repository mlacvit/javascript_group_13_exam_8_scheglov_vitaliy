import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { EditComponent } from './edit/edit.component';
import { NewQuoteComponent } from './new-quote/new-quote.component';
import { QuoteComponent } from './quote/quote.component';
import { CategoryComponent } from './category/category.component';

const routes: Routes = [
  {path: '', component: HomeComponent, children: [
      {path: 'edit', component: EditComponent},
      {path: 'new', component: NewQuoteComponent},
      {path: 'quote', component: QuoteComponent},
      {path: ':id', component: CategoryComponent},
    ]},
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
