import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { QuoteModal } from '../quote.modal';
import { QuoteServices } from '../quote.services';
import { map } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  quote: QuoteModal[] | null = null;
  category!: '';
  constructor(public service: QuoteServices) {
  }

  ngOnInit(): void {
      this.service.allQuote()
    }

}
